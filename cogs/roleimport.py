import discord
from discord.ext.commands import Cog
from discord.ext import commands


class RoleImport(Cog):
    def __init__(self, bot):
        self.bot = bot

    def user_rolebot_roles(self, user: discord.Member):
        """Returns the list of rolebot roles of a Member. (except pronouns)"""
        user_rroles = []
        for role in user.roles:
            if role.name.startswith("rolebot-"):
                user_rroles.append(role)
        return user_rroles

    @commands.bot_has_permissions(manage_roles=True)
    @commands.guild_only()
    @commands.command(name="import")
    @commands.cooldown(1, 60, commands.BucketType.user)
    async def _import(self, ctx, guild_id: int):
        """Imports rolebot roles from another guild.

        This command can only be used every 60 secs per user."""

        mentioned_guild = self.bot.get_guild(guild_id)
        guild_member = None
        if mentioned_guild in self.bot.guilds:
            maybe_member = mentioned_guild.get_member(ctx.author.id)
            if maybe_member:
                guild_member = maybe_member
            else:
                return await ctx.send(f"{ctx.author.mention}: You're not in that guild")
        else:
            return await ctx.send(f"{ctx.author.mention}: I'm not in that guild")
        self.bot.log.info(guild_member)

        add_counter = 0

        pronoun_names = [
            pronoun.name for pronoun in self.bot.user_pronouns(guild_member)
        ]
        rroles = self.user_rolebot_roles(guild_member)

        # Remove all rolebot roles and pronouns the user has
        await self.bot.remove_all_rolebot_color_roles(ctx.author)
        await self.bot.remove_all_user_pronoun_roles(ctx.author)

        # Do local max pronoun role per user count checks
        pronoun_limit = self.bot.query_setting("maxpronouns", ctx.guild.id)
        if len(pronoun_names) > pronoun_limit:
            return await ctx.send(
                f"{ctx.author.mention}: You have "
                "more pronouns than the limit, I can't "
                "import this."
            )

        pronouns = self.bot.query_setting("pronouns", ctx.guild.id)

        # Add roles from the other guild
        for pronoun in pronoun_names:
            # Check if the pronoun is allowed in this guild
            if pronoun in pronouns:
                pronoun_role = await self.bot.get_role(ctx, pronoun, False)
                await ctx.author.add_roles(pronoun_role)
                add_counter += 1

        for rrole in rroles:
            role_name = rrole.name
            role_color = rrole.color
            # Account for color roles so we can avoid importing them on guilds
            # where they're disabled
            if rrole.name.lower().startswith("rolebot-#"):
                role_color = rrole.name.lower().replace("rolebot-#", "").strip()
                check_result = self.bot.check_color(ctx.guild.id, role_color)

                # If the color role throws an error for a reason, skip it
                if check_result:
                    continue

                # This can be broken if user tries to but that won't lead to
                # an improper role being added, the command will simply error.
                # I'm intentionally not adding checks for this.
                role_color = discord.Color(int(role_color), 16)
                role_name = f"rolebot-{str(role_color)}"
            else:
                # If this isn't the case, then it's a malicious/manual role.
                # Don't add it.
                # The reason I'm adding this if block at all is so that I can
                # add more role types easily in the future.
                continue

            role = await self.bot.get_role(ctx, role_name, True, True, role_color)
            if not role:
                return await ctx.send(
                    f"{ctx.author.mention}: Guild "
                    "reached max role limit, can't import."
                )
            await ctx.author.add_roles(role)
            add_counter += 1

        await ctx.send(f"{ctx.author.mention}: Done, {add_counter} role(s) added.")


def setup(bot):
    bot.add_cog(RoleImport(bot))
