from discord.ext import commands
from discord.ext.commands import Cog


class Pronoun(Cog):
    def __init__(self, bot):
        self.bot = bot

    @commands.command()
    @commands.cooldown(1, 30, commands.BucketType.channel)
    async def pronounlist(self, ctx):
        """Gets the list of self-assignable pronouns.

        This list can only be get every 30 secs per channel."""
        pronouns = self.bot.query_setting("pronouns", ctx.guild.id)
        overriden = self.bot.setting_overriden("pronouns", ctx.guild.id)
        notice = "Are your pronouns not listed here?\n"
        if overriden:
            notice += (
                "Your guild maintains its own overrides for the list. "
                "Please request staff to whitelist your pronoun."
            )
        else:
            notice += (
                "Please open an issue or send a PR on "
                "<https://gitlab.com/a/rolebot> to get whitelisted."
            )

        swooshy_pronouns = ", ".join(pronouns)
        await ctx.send(
            "List of self-assignable pronouns in your guild:\n"
            f"```\n{swooshy_pronouns}```\n\n{notice}"
        )

    @commands.bot_has_permissions(manage_roles=True)
    @commands.guild_only()
    @commands.command()
    @commands.cooldown(1, 30, commands.BucketType.user)
    async def clearpronouns(self, ctx):
        """Clears your pronoun roles.

        This list can only be used every 60 secs per user."""
        await self.bot.remove_all_user_pronoun_roles(ctx.author)
        return await ctx.send(f"{ctx.author.mention}: Done")

    @commands.bot_has_permissions(manage_roles=True)
    @commands.guild_only()
    @commands.command(aliases=["pronoun"])
    @commands.cooldown(1, 3, commands.BucketType.user)
    async def pronounme(self, ctx, *, pronoun: str):
        """Adds/removes a pronoun role (see rb!pronounlist).

        This command can only be used every 10 seconds per user."""

        pronoun_limit = self.bot.query_setting("maxpronouns", ctx.guild.id)
        overriden = self.bot.setting_overriden("pronouns", ctx.guild.id)
        notice = "Sorry! This pronoun is not whitelisted.\n"
        if overriden:
            notice = (
                "Your guild maintains its own overrides for the list. "
                "Please request staff to whitelist your pronoun."
            )
        else:
            notice += (
                "Please open an issue or send a PR on "
                "<https://gitlab.com/a/rolebot> to get whitelisted."
            )

        pronoun = pronoun.strip("\" '")
        pronoun_count = len(self.bot.user_pronouns(ctx.author))
        safe_name = str(ctx.author).replace("@", "")
        role_name = self.bot.query_pronoun(pronoun, ctx.guild.id)
        if not role_name:
            return await ctx.send(f"{safe_name}: {notice}")
        elif pronoun_count >= pronoun_limit:
            return await ctx.send(
                f"{safe_name}: "
                f"Sorry, but I only assign {pronoun_limit} "
                "pronouns per user to prevent "
                "abuse.\n\nTo remove"
                " your pronoun(s), you can do "
                "`rb!clearpronouns` or "
                "`rb!pronounme <pronoun-to-remove>`.\n"
                "Also, you can ask your guild's staff to "
                "set an override on `maxpronoun` config."
            )

        pronoun_role = await self.bot.get_role(ctx, role_name, False)
        if pronoun_role in ctx.author.roles:
            await ctx.author.remove_roles(pronoun_role)
            await self.bot.remove_role_if_no_users(pronoun_role)
            return await ctx.send(f"{safe_name}: Removed pronoun {role_name}.")

        await ctx.author.add_roles(pronoun_role)
        await ctx.send(f"{safe_name}: Pronoun {role_name} added!")


def setup(bot):
    bot.add_cog(Pronoun(bot))
