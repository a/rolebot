from discord.ext import commands
from discord.ext.commands import Cog
from helpers.guildconfig import get_overrides, set_override


class Overrides(Cog):
    def __init__(self, bot):
        self.bot = bot
        self.listkeys = ["colors", "pronouns", "custom"]

    @commands.guild_only()
    @commands.command()
    @commands.cooldown(1, 30, commands.BucketType.channel)
    async def settings(self, ctx):
        """Gets the default config settings.

        This list can only be retrieved every 30 secs per channel."""
        defaults_conf = repr(self.bot.config["defaults"])
        overriden_conf = repr(get_overrides(ctx.guild.id))
        await ctx.send(
            f"Default settings:\n"
            f"```python\n{defaults_conf}```\n\n"
            f"Guild overrides:\n"
            f"```python\n{overriden_conf}```\n\n"
            "You may use override command to set/remove overrides."
        )

    @commands.guild_only()
    @commands.command()
    @commands.cooldown(1, 15, commands.BucketType.channel)
    async def override(self, ctx, key, *value):
        """Sets a config override, requires manage roles.

        See settings command to see what you can change.

        Some examples:
        - "override custom landing in la"
        sets allowed custom roles to landing, in and la
        - "override colors #dabdab"
        sets allowed colors to just #dabdab
        - "override maxroles 200"
        sets maximum color roles to 200

        - "override colors disable"
        disables color commands
        - "override colors enable"
        enables any colors to be used by users
        - "override colors default"
        sets the colors to the default rolebot settings

        This command can only be used every 15 secs per channel."""
        user_has_perms = await self.bot.owner_or_manage_roles(ctx)
        if not user_has_perms:
            return await ctx.send(
                f"{ctx.author.mention}: "
                "To be able to set overrides, "
                "you need Manage Roles permission."
            )

        defaults = self.bot.config["defaults"]

        if key not in defaults:
            return await ctx.send(
                f"{ctx.author.mention}: No such key. See settings command."
            )

        # Oh my god this is so hacky
        if len(value) is 1:
            value = value[0]
            if value.lower() in ["true", "enable"]:
                value = True
            elif value.lower() in ["false", "disable"]:
                if key in self.listkeys:
                    value = []
                else:
                    value = False
            elif value.lower() in ["none", "reset", "default"]:
                value = None
            elif value.isdigit():
                value = int(value)

            if key in self.listkeys and value not in [None, [], True]:
                value = [value]
        else:
            value = list(value)

        self.bot.config = set_override(ctx.guild.id, key, value)
        await ctx.send(f"`{key}` is now `{repr(value)}`")


def setup(bot):
    bot.add_cog(Overrides(bot))
