import discord
from discord.ext.commands import Cog
from discord.ext import commands
import colorsys


class CustomColor(Cog):
    def __init__(self, bot):
        self.bot = bot

    @commands.guild_only()
    @commands.command(aliases=["servercolors", "servercolours", "guildcolours"])
    @commands.cooldown(1, 30, commands.BucketType.channel)
    async def guildcolors(self, ctx):
        """Gets the current colors of the guild.

        This list can only be get every 30 secs per channel."""

        colorsort = lambda color: colorsys.rgb_to_hsv(
            *(int(color[1:][i : i + 2], 16) / 255.0 for i in range(0, 5, 2))
        )

        allowedcolors = self.bot.query_setting("colors", ctx.guild.id)
        # heheheh hacky code
        if allowedcolors is True:
            swooshy_allowed = "Any"
        elif not allowedcolors:
            swooshy_allowed = "None"
        else:
            swooshy_allowed = (
                f"```\n{', '.join(sorted(allowedcolors, key=colorsort))}\n```"
            )

        # heheheh more hacky code
        guildcolors = self.bot.rolebot_guild_colors(ctx)
        swooshy_guild_colors = (
            ", ".join(sorted(guildcolors, key=colorsort)) if guildcolors else "-"
        )

        limit = self.bot.query_setting("maxroles", ctx.guild.id)
        await ctx.send(
            f"{len(guildcolors)} color(s):\n"
            f"```\n{swooshy_guild_colors}\n```\n"
            f"Color role limit for this guild is: {limit}\n"
            f"Allowed colors: {swooshy_allowed}"
        )

    @commands.bot_has_permissions(manage_roles=True)
    @commands.guild_only()
    @commands.command(aliases=["colourme", "color", "colour"])
    @commands.cooldown(1, 5, commands.BucketType.user)
    async def colorme(self, ctx, color: discord.Color, user: discord.Member = None):
        """Gives you a custom color role, takes hex or named colors.

        See the factory methods on
        https://discordpy.readthedocs.io/en/latest/api.html#discord.Colour
        for a list of named colors.

        Also, you can use the syntax "rb!color <color> <user>" if you have
        Manage Roles permission to set another user's color.

        This command can only be used every 10 seconds per user."""

        if not user:
            user = ctx.author
        user_has_perms = await self.bot.owner_or_manage_roles(ctx)
        if user != ctx.author and not user_has_perms:
            return await ctx.send(
                f"{ctx.author.mention}: "
                "To be able to set others' "
                "colors, you need Manage Roles permission."
            )

        color_hex = str(color)

        # Check if guild allows this role
        check_result = self.bot.check_color(ctx.guild.id, color_hex)

        if check_result:
            return await ctx.send(f"{ctx.author.mention}: {check_result}")

        role_name = f"rolebot-{color_hex}"
        color_role = await self.bot.get_role(ctx, role_name, True, True, color)

        if color_role in user.roles:
            await user.remove_roles(color_role)
            await self.bot.remove_role_if_no_users(color_role)
            return await ctx.send(f"{user.mention}: Removed color.")
        elif not color_role:
            max_limit = self.bot.query_setting("maxroles", ctx.guild.id)
            return await ctx.send(
                f"{user.mention}: "
                "RoleBot reached max color limit "
                f"of {max_limit}."
            )

        await self.bot.remove_all_rolebot_color_roles(user)

        await user.add_roles(color_role)
        msg = f"Successfully set role color for {user.mention}."
        embed = discord.Embed(colour=color, description=msg)
        await ctx.send(embed=embed)


def setup(bot):
    bot.add_cog(CustomColor(bot))
