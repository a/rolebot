import json


def get_config():
    with open("config.json", "r") as f:
        return json.load(f)


def set_config(contents):
    with open("config.json", "w") as f:
        f.write(contents)


def get_overrides(guild_id):
    config = get_config()
    guild_id = str(guild_id)
    if guild_id not in config["overrides"]:
        return {}
    else:
        return config["overrides"][guild_id]


def set_override(guild_id, key, value):
    config = get_config()
    guild_id = str(guild_id)
    if guild_id not in config["overrides"]:
        config["overrides"][guild_id] = {key: value}

    # handle override deletions
    if value is None:
        del config["overrides"][guild_id][key]
        set_config(json.dumps(config))
        return config

    config["overrides"][guild_id][key] = value
    set_config(json.dumps(config))
    return config
